package com.ignite.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @author alex
 */
@JsonIgnoreProperties("id")
public class Endorsement extends AuthorizedRequest {

    private long id;
    private String policyNumber;
    private String endorsementNumber;
    private Date endorsendDate;
    private String endorsendType;
    private Date fromDate;
    private Date toDate;
    private String sharedBy;
    private BigDecimal premium;

    public String getPolicyNumber() {
        return policyNumber;
    }

    public void setPolicyNumber(String policyNumber) {
        this.policyNumber = policyNumber;
    }

    public String getEndorsementNumber() {
        return endorsementNumber;
    }

    public void setEndorsementNumber(String endorsementNumber) {
        this.endorsementNumber = endorsementNumber;
    }

    public Date getEndorsendDate() {
        return endorsendDate;
    }

    public void setEndorsendDate(Date endorsendDate) {
        this.endorsendDate = endorsendDate;
    }

    public String getEndorsendType() {
        return endorsendType;
    }

    public void setEndorsendType(String endorsendType) {
        this.endorsendType = endorsendType;
    }

    public Date getFromDate() {
        return fromDate;
    }

    public void setFromDate(Date fromDate) {
        this.fromDate = fromDate;
    }

    public Date getToDate() {
        return toDate;
    }

    public void setToDate(Date toDate) {
        this.toDate = toDate;
    }

    public String getSharedBy() {
        return sharedBy;
    }

    public void setSharedBy(String sharedBy) {
        this.sharedBy = sharedBy;
    }

    public BigDecimal getPremium() {
        return premium;
    }

    public void setPremium(BigDecimal premium) {
        this.premium = premium;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }
}
