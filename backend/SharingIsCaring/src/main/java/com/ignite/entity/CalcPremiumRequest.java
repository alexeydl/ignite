package com.ignite.entity;

import java.util.Date;

/**
 * Request for premium calculation
 * @author alex
 */
public class CalcPremiumRequest extends AuthorizedRequest{

    private Date dateStart;
    private Date dateEnd;
    private String countryFrom;
    private String countryTo;

    public Date getDateStart() {
        return dateStart;
    }

    public void setDateStart(Date dateStart) {
        this.dateStart = dateStart;
    }

    public Date getDateEnd() {
        return dateEnd;
    }

    public void setDateEnd(Date dateEnd) {
        this.dateEnd = dateEnd;
    }

    public String getCountryFrom() {
        return countryFrom;
    }

    public void setCountryFrom(String countryFrom) {
        this.countryFrom = countryFrom;
    }

    public String getCountryTo() {
        return countryTo;
    }

    public void setCountryTo(String countryTo) {
        this.countryTo = countryTo;
    }
}
