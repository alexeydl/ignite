package com.ignite.entity;

import java.math.BigDecimal;

public class PolicyFullInfo extends Policy{
	
	private int count;
	private BigDecimal savings;
	public int getCount() {
		return count;
	}
	public void setCount(int count) {
		this.count = count;
	}
	public BigDecimal getSavings() {
		return savings;
	}
	public void setSavings(BigDecimal savings) {
		this.savings = savings;
	}
	
	
	

}
