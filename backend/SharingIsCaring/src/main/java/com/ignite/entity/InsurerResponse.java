package com.ignite.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Insurer
 * @author alex
 */
@JsonIgnoreProperties("id")
public class InsurerResponse {

    private int id;
    private String name;
    private String code;
    private String picture;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }
}

