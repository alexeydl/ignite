package com.ignite.entity;

/**
 * Response for create new policy method
 * @author alex
 */
public class PolicyCreateResponse {

    private String policyNumber;

    public String getPolicyNumber() {
        return policyNumber;
    }

    public void setPolicyNumber(String policyNumber) {
        this.policyNumber = policyNumber;
    }
}
