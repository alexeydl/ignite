package com.ignite.entity;

import java.util.Date;

/**
 * Request for sharing period
 * @author alex
 */
public class SharingPeriodRequest extends AuthorizedRequest {

    private long id;
    private Date fromDate;
    private Date toDate;
    private String endorsedType;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Date getFromDate() {
        return fromDate;
    }

    public void setFromDate(Date fromDate) {
        this.fromDate = fromDate;
    }

    public Date getToDate() {
        return toDate;
    }

    public void setToDate(Date toDate) {
        this.toDate = toDate;
    }

    public String getEndorsedType() {
        return endorsedType;
    }

    public void setEndorsedType(String endorsedType) {
        this.endorsedType = endorsedType;
    }
}
