package com.ignite.entity;

/**
 * @author alex
 */
public class AuthResponse {

    private String session;

    public String getSession() {
        return session;
    }

    public void setSession(String session) {
        this.session = session;
    }
}
