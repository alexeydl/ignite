package com.ignite.entity;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @author alex
 */
public class SharingHistoryResponse {

    private Date fromDate;
    private Date toDate;
    private BigDecimal saved;
    private BigDecimal claimed;
    private int claims;
    private int shard;

    public Date getFromDate() {
        return fromDate;
    }

    public void setFromDate(Date fromDate) {
        this.fromDate = fromDate;
    }

    public Date getToDate() {
        return toDate;
    }

    public void setToDate(Date toDate) {
        this.toDate = toDate;
    }

    public BigDecimal getSaved() {
        return saved;
    }

    public void setSaved(BigDecimal saved) {
        this.saved = saved;
    }

    public BigDecimal getClaimed() {
        return claimed;
    }

    public void setClaimed(BigDecimal claimed) {
        this.claimed = claimed;
    }

    public int getClaims() {
        return claims;
    }

    public void setClaims(int claims) {
        this.claims = claims;
    }

    public int getShard() {
        return shard;
    }

    public void setShard(int shard) {
        this.shard = shard;
    }
}
