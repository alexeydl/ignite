package com.ignite.entity;

/**
 * Request for sharing Policy
 * @author alex
 */
public class SharePolicyRequest extends AuthorizedRequest {

    private String policyNumber;
    private String sharingStatus;

    public String getPolicyNumber() {
        return policyNumber;
    }

    public void setPolicyNumber(String policyNumber) {
        this.policyNumber = policyNumber;
    }

    public String getSharingStatus() {
        return sharingStatus;
    }

    public void setSharingStatus(String sharingStatus) {
        this.sharingStatus = sharingStatus;
    }
}
