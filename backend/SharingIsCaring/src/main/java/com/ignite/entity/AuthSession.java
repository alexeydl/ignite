package com.ignite.entity;

import java.util.Date;

/**
 * Auth session
 * @author alex
 *
 */
public class AuthSession {
	
	private String login;
	private String sessionHash;
	private Date expireDate;
	private String fullName;

	/**
	 * @return the login
	 */
	public String getLogin() {
		return login;
	}
	/**
	 * @param login the login to set
	 */
	public void setLogin(String login) {
		this.login = login;
	}
	/**
	 * @return the sessionHash
	 */
	public String getSessionHash() {
		return sessionHash;
	}
	/**
	 * @param sessionHash the sessionHash to set
	 */
	public void setSessionHash(String sessionHash) {
		this.sessionHash = sessionHash;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((login == null) ? 0 : login.hashCode());
		result = prime * result + ((sessionHash == null) ? 0 : sessionHash.hashCode());
		return result;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AuthSession other = (AuthSession) obj;
		if (login == null) {
			if (other.login != null)
				return false;
		} else if (!login.equals(other.login))
			return false;
		if (sessionHash == null) {
			if (other.sessionHash != null)
				return false;
		} else if (!sessionHash.equals(other.sessionHash))
			return false;
		return true;
	}
	/**
	 * @return the expireDate
	 */
	public Date getExpireDate() {
		return expireDate;
	}
	/**
	 * @param expireDate the expireDate to set
	 */
	public void setExpireDate(Date expireDate) {
		this.expireDate = expireDate;
	}
}
