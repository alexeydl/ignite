package com.ignite.entity;

import java.math.BigDecimal;
import java.util.Date;

/**
 * Request to create a new policy
 * @author alex
 */
public class PolicyCreateRequest extends AuthorizedRequest {

	private String policyType;
    private String sharingClause;
    private String fromCountry;
    private String toCountry;
    private Date inceptionDate;
    private Date expireDate;
    private BigDecimal premium;
    private String sharingStatus;
    private String basedPolicy;

    public String getSharingClause() {
        return sharingClause;
    }

    public void setSharingClause(String sharingClause) {
        this.sharingClause = sharingClause;
    }

    public String getFromCountry() {
        return fromCountry;
    }

    public void setFromCountry(String fromCountry) {
        this.fromCountry = fromCountry;
    }

    public String getToCountry() {
        return toCountry;
    }

    public void setToCountry(String toCountry) {
        this.toCountry = toCountry;
    }

    public Date getInceptionDate() {
        return inceptionDate;
    }

    public void setInceptionDate(Date inceptionDate) {
        this.inceptionDate = inceptionDate;
    }

    public Date getExpireDate() {
        return expireDate;
    }

    public void setExpireDate(Date expireDate) {
        this.expireDate = expireDate;
    }

    public BigDecimal getPremium() {
        return premium;
    }

    public void setPremium(BigDecimal premium) {
        this.premium = premium;
    }

    public String getSharingStatus() {
        return sharingStatus;
    }

    public void setSharingStatus(String sharingStatus) {
        this.sharingStatus = sharingStatus;
    }

	public String getPolicyType() {
		return policyType;
	}

	public void setPolicyType(String policyType) {
		this.policyType = policyType;
	}

	public String getBasedPolicy() {
		return basedPolicy;
	}

	public void setBasedPolicy(String basedPolicy) {
		this.basedPolicy = basedPolicy;
	}
}
