package com.ignite.entity;

import java.math.BigDecimal;

public class CalculationResponse {
	
	private BigDecimal communtiyPrem;
	private BigDecimal libertyPrem;
	private BigDecimal aigPrem;
	private BigDecimal geicoPrem;
	private String basedNumber;
	
	/**
	 * @return the communtiyPrem
	 */
	public BigDecimal getCommuntiyPrem() {
		return communtiyPrem;
	}
	/**
	 * @param communtiyPrem the communtiyPrem to set
	 */
	public void setCommuntiyPrem(BigDecimal communtiyPrem) {
		this.communtiyPrem = communtiyPrem;
	}
	/**
	 * @return the libertyPrem
	 */
	public BigDecimal getLibertyPrem() {
		return libertyPrem;
	}
	/**
	 * @param libertyPrem the libertyPrem to set
	 */
	public void setLibertyPrem(BigDecimal libertyPrem) {
		this.libertyPrem = libertyPrem;
	}
	/**
	 * @return the aigPrem
	 */
	public BigDecimal getAigPrem() {
		return aigPrem;
	}
	/**
	 * @param aigPrem the aigPrem to set
	 */
	public void setAigPrem(BigDecimal aigPrem) {
		this.aigPrem = aigPrem;
	}
	/**
	 * @return the geicoPrem
	 */
	public BigDecimal getGeicoPrem() {
		return geicoPrem;
	}
	/**
	 * @param geicoPrem the geicoPrem to set
	 */
	public void setGeicoPrem(BigDecimal geicoPrem) {
		this.geicoPrem = geicoPrem;
	}
	public String getBasedNumber() {
		return basedNumber;
	}
	public void setBasedNumber(String basedNumber) {
		this.basedNumber = basedNumber;
	}
	
	

}
