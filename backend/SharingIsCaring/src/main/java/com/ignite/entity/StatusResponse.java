package com.ignite.entity;

/**
 *  Common status
 *  @author alex
 */
public class StatusResponse {

    private boolean status;

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }
}
