package com.ignite.security;

import java.util.HashMap;
import java.util.Map;
import javax.annotation.PostConstruct;
import org.springframework.stereotype.Component;
import com.ignite.entity.AuthSession;

/**
 * 
 * @author alex
 */
@Component
public class TokenBasedSecurityContext {
	
	//FIXME
	private final String DEFAULT_SESSION_HASH="3454567543";
	private final String DEFAULT_SESSION_HASH_1="4363697827";
	private Map<String,AuthSession> activeSessions;
	
	@PostConstruct
	public void init() {
		activeSessions = new HashMap<String,AuthSession>();
		AuthSession defaultSession = new AuthSession();

		// FIXME:
		defaultSession = new AuthSession();
		defaultSession.setLogin("contact.hungle@gmail.com");
		defaultSession.setSessionHash(DEFAULT_SESSION_HASH);
		defaultSession.setExpireDate(null);
		defaultSession.setFullName("Insurance Trend Setters");

		activeSessions.put(DEFAULT_SESSION_HASH, defaultSession);
	}
	
	/**
	 * 
	 * @param sessionHash
	 * @return
	 */
	public AuthSession getSession(String sessionHash) {
		return activeSessions.get(sessionHash);
	}
	
	/**
	 * 
	 * @param session
	 */
	public void addActiveSession(AuthSession session) {
		activeSessions.put(session.getSessionHash(), session);
	}
}
