package com.ignite.service;

import java.util.Collection;
import java.util.List;

import com.ignite.entity.*;
import org.springframework.stereotype.Service;

/**
 * 
 * @author alex
 *
 */
@Service
public interface CommonDataService {

	/**
	 * 
	 * @return
	 */
	List<InsurerResponse> getInsurersForSharing();

	/**
	 * Line of business
	 * @param insurer_code Insurer code
	 * @return
	 */
	List<LineOfBusinessResponse> getLobsByInsurer(String insurer_code);

	/**
	 * 
	 * @param policy_number
	 * @param username
	 * @return
	 */
	StatusResponse getPolicyAllowedStatus(String policy_number, String username);

	/**
	 * Share policy by request rq, or stop sharing
	 * @param rq
	 */
	void updatePolicySharing(SharePolicyRequest rq, AuthSession session);

	/**
	 *
	 * @param number
	 * @return
	 */
	Policy getPolicyByNumber(String number);


	/**
	 *
	 * @return all sharing history
	 */
	Collection<Endorsement> getAllSharingHistory();

	/**
	 * 
	 * @return All policies 
	 */
	List<Policy> getAllPolicies();

	/**
	 * Create new policy (communtiy)
	 * @param policy
	 * @return Policy number
	 */
	String createNewCommunityPolicy(Policy policy);

	/**
	 * 
	 * @param policy
	 * @return
	 */
	String createNewLibertyPolicy(Policy policy);
	
	String generateNewEndorosmentNumber (String policyNumber);
	
	void saveIndorosment(Endorsement info);

}
