package com.ignite.service;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.filter.HTTPBasicAuthFilter;
import com.sun.jersey.core.util.MultivaluedMapImpl;
import org.springframework.stereotype.Service;
import javax.ws.rs.core.MediaType;


/**
 * Service for sending emails
 * @author alex
 */
@Service
public class MailService {

    private final static String API_KEY = "key-58d17511e707f4f5898682bf36da7aa0";
    private final static String DOMAIN_NAME = "sandboxeb31b3f6c6b8440491fe78b055e2853c.mailgun.org";
    private final static String USER = "Insure Share";

    /**
     * Send email message
     * @param text Text
     * @param email Email
     * @return true if success
     */
    public boolean sendEmail(String text, String email, String subject) {

        try {

            Client client = Client.create();
            client.addFilter(new HTTPBasicAuthFilter("api", API_KEY));
            WebResource webResource =
                    client.resource("https://api.mailgun.net/v3/"+ DOMAIN_NAME +
                            "/messages");
            MultivaluedMapImpl formData = new MultivaluedMapImpl();
            formData.add("from", USER + " <mailgun@"+DOMAIN_NAME+">");
            formData.add("to", email);
            formData.add("subject", subject);
            formData.add("text", text);
            webResource.type(MediaType.APPLICATION_FORM_URLENCODED).
                    post(ClientResponse.class, formData);

            return true;

        } catch (Exception e){
            e.printStackTrace();
        }
        return false;
    }


}
