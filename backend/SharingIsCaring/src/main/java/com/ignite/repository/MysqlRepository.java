package com.ignite.repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;

import com.ignite.entity.*;
import com.ignite.service.MailService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import com.ignite.service.CommonDataService;
import com.ignite.settings.AppProperties;

/**
 * MYSQL repository
 * @author alex
 *
 */

@Component
public class MysqlRepository  implements CommonDataService {
	
	private final Logger log = LoggerFactory.getLogger(MysqlRepository.class);
	@Autowired
	private DataSource dataSource;
	@Autowired
	private AppProperties props;
	@Autowired
	private MailService mailService;
	private JdbcTemplate jdbcTemplate;
	
	@PostConstruct
	public void init(){
		jdbcTemplate = new JdbcTemplate(dataSource);
	}
	
	public AppProperties getProps() {
		return props;
	}

	public void setProps(AppProperties props) {
		this.props = props;
	}

	public DataSource getDataSource() {
		return dataSource;
	}

	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
	}
	
	public MailService getMailService() {
		return mailService;
	}

	public void setMailService(MailService mailService) {
		this.mailService = mailService;
	}

	
	
	@Override
	public List<InsurerResponse> getInsurersForSharing() {
		
		List<InsurerResponse> result = new ArrayList<InsurerResponse>();
		List<Map<String, Object>> rows = jdbcTemplate.queryForList("SELECT * FROM ALLOWED_INSURERS");
		
		for (Map<String, Object> row : rows) {
			InsurerResponse obj = new InsurerResponse();
			
			obj.setId((int) row.get("ID"));
			obj.setCode((String) row.get("CODE"));
			obj.setName((String) row.get("NAME"));
			obj.setPicture(props.getProperty("HOST") + "resources/" + (String) row.get("PICTURE_LINK"));
			
			result.add(obj);
		}
		
		return result;
	}


	@Override
	public List<LineOfBusinessResponse> getLobsByInsurer(String insurer_code) {
		
		List<LineOfBusinessResponse> result = new ArrayList<LineOfBusinessResponse>();
		List<Map<String, Object>> rows = jdbcTemplate
				.queryForList("SELECT LOB_CODE FROM ALLOWED_LOBS_BY_INSURER WHERE INSURER_CODE='" + insurer_code+"'");
		
		for (Map<String, Object> row : rows) {
			LineOfBusinessResponse obj = getLobByCode((String) row.get("LOB_CODE"));
			result.add(obj);
		}
		
		return result;
	}
	
	private LineOfBusinessResponse getLobByCode(String code) {
		
		return jdbcTemplate.queryForObject("SELECT * FROM ALLOWED_LOBS WHERE LOB_CODE='" + code + "'",
				new RowMapper<LineOfBusinessResponse>() {

					@Override
					public LineOfBusinessResponse mapRow(ResultSet rs, int arg1) throws SQLException {
						
						LineOfBusinessResponse result = new LineOfBusinessResponse();
						
						result.setId(rs.getInt("ID"));
						result.setName(rs.getString("NAME"));
						result.setCode(rs.getString("LOB_CODE"));
						result.setPicture(props.getProperty("HOST") + "resources/" + rs.getString("PICTURE_LINK"));
						
						return result;
					}
			
		});
	}

	@Override
	public StatusResponse getPolicyAllowedStatus(String policy_number, String username) {
		
		StatusResponse result = new StatusResponse();
		
		List<Map<String, Object>> rows = jdbcTemplate.queryForList("SELECT * FROM ALLOWED_POLICIES");
		
		for (Map<String, Object> row : rows) {
			
			String holder = (String) row.get("POLICY_HOLDER");
			String cause = (String) row.get("SHARING_CAUSE");
			String number = (String) row.get("NUMBER");
			
			if ("Y".equalsIgnoreCase(cause) && policy_number.trim().equalsIgnoreCase(number.trim())
					&& username.trim().equalsIgnoreCase(holder.trim())) {
				result.setStatus(true);
				return result;
			}
		}
		
		return result;
	}

	@Override
	public void updatePolicySharing(SharePolicyRequest rq, AuthSession session) {
		
		boolean startSharing = rq.getSharingStatus().trim().equalsIgnoreCase("Y");

		Policy currentPolicy = this.getPolicyByNumber(rq.getPolicyNumber());

		//UDATE POLICY STATUS
		jdbcTemplate.update("UPDATE ALLOWED_POLICIES SET SHARING_STATUS = ? WHERE NUMBER = ?", startSharing ? "Y" : "N",
				rq.getPolicyNumber().toUpperCase());

		Calendar c = Calendar.getInstance();
		c.setTime(new Date());
		c.add(Calendar.DATE, 1);

		//add new record into endorsement table
		jdbcTemplate.update("INSERT INTO SHARING_ENDORSEMENT_INFO (POLICY_NUMBER,ENDR_NUMBER,ENDR_DATE,ENDR_TYPE,FROM_DATE,TO_DATE,SHARED_BY,PREMIUM)"
								+ "VALUES (?, ?, ?, ? , ?, ?, ?, ?)",
				currentPolicy.getPolicyNumber() , generateNewEndorosmentNumber(currentPolicy.getPolicyNumber()),
				new Date(),  startSharing ? "Share" : "NoShare", c.getTime(), currentPolicy.getExpireDate(), null, null );

		if (startSharing) {
			
			final String EMAIL_TEXT = "Dear " + session.getFullName() + ",\n sharing for policy "
					+ rq.getPolicyNumber()
					+ " has been confirmed.\n";

			mailService.sendEmail(EMAIL_TEXT, currentPolicy.getPolicyHolder(), "Your policy shared now!");
			
		}
		
	}
	

	/**
	 *
	 * @param number
	 * @return
	 */
	@Override
	public Policy getPolicyByNumber(final String number) {
		
		for (Policy buff: getAllPolicies()){
			if (buff.getPolicyNumber().trim().equalsIgnoreCase(number.trim()))
				return buff;
		}
		return null;
	}

	@Override
	public Collection<Endorsement> getAllSharingHistory() {

		List<Endorsement> allHistory = jdbcTemplate.query("SELECT * FROM SHARING_ENDORSEMENT_INFO", new RowMapper<Endorsement>() {

			@Override
			public Endorsement mapRow(ResultSet resultSet, int i) throws SQLException {

				Endorsement result = new Endorsement();

				result.setId(resultSet.getLong("ID"));
				result.setPolicyNumber(resultSet.getString("POLICY_NUMBER"));
				result.setEndorsementNumber(resultSet.getString("ENDR_NUMBER"));

				Calendar calendar = Calendar.getInstance();
				calendar.setTimeInMillis(resultSet.getTimestamp("ENDR_DATE").getTime());
				result.setEndorsendDate(calendar.getTime());

				result.setEndorsendType(resultSet.getString("ENDR_TYPE"));

				calendar.setTimeInMillis(resultSet.getTimestamp("FROM_DATE").getTime());
				result.setFromDate(calendar.getTime());

				calendar.setTimeInMillis(resultSet.getTimestamp("TO_DATE").getTime());
				result.setToDate(calendar.getTime());

				result.setSharedBy(resultSet.getString("SHARED_BY"));
				result.setPremium(resultSet.getBigDecimal("PREMIUM"));

				return result;
			}
		});

		return allHistory;

	}

	/**
	 *
	 * Generate new endorsement number based on policy number
	 * and endorsement history
	 * Expected endorsement number <POL-NO-xxx>
	 * @param policyNumber
	 * @return Endorsement number
	 */
	public String generateNewEndorosmentNumber (String policyNumber) {
		
		if (policyNumber == null || policyNumber.isEmpty())
			return "UNKNOWN";

		Collection<Endorsement> history = getAllSharingHistory();

		int prevEndorNumber = 0;

		for (Endorsement buff: history){

			if (buff.getEndorsementNumber().contains(policyNumber.trim())){

				String endNumber = buff.getEndorsementNumber().substring(buff.getEndorsementNumber().length() - 3);
				Integer number = Integer.valueOf(endNumber);
				if (prevEndorNumber < number.intValue()) {
					prevEndorNumber = number;
				}
			}

		}

		return policyNumber + "-" + String.format("%03d", prevEndorNumber + 1);
	}

	@Override
	public List<Policy> getAllPolicies() {
		List<Policy> result = this.getAllAllowedPolicies();
		result.addAll(this.getAllCommunityPolicies());
		return result;
	}

	private List<Policy> getAllAllowedPolicies() {
		return selectAllPoliciesFromTable("ALLOWED_POLICIES");
	}
	
	private List<Policy> getAllCommunityPolicies() {
		return selectAllPoliciesFromTable("COMMUNITY_POLICIES");
	}
	
	private List<Policy> selectAllPoliciesFromTable(String tableName) {
		
		return jdbcTemplate.query("SELECT * FROM " + tableName, new RowMapper<Policy>() {

			@Override
			public Policy mapRow(ResultSet resultSet, int i) throws SQLException {

				Policy result = new Policy();

				result.setId(resultSet.getLong("ID"));
				result.setPolicyNumber(resultSet.getString("NUMBER"));

				result.setSharingClause(resultSet.getString("SHARING_CAUSE"));
				result.setFromCountry(resultSet.getString("FROM_COUNTRY"));
				result.setToCountry(resultSet.getString("TO_COUNTRY"));

				Calendar calendar = Calendar.getInstance();
				calendar.setTimeInMillis(resultSet.getTimestamp("INCEPTION_DATE").getTime());
				result.setInceptionDate(calendar.getTime());

				calendar.setTimeInMillis(resultSet.getTimestamp("EXPIRE_DATE").getTime());
				result.setExpireDate(calendar.getTime());
				result.setPolicyHolder(resultSet.getString("POLICY_HOLDER"));
				result.setPremium(resultSet.getBigDecimal("PREMIUM"));
				result.setSharingStatus(resultSet.getString("SHARING_STATUS"));

				return result;
			}
		});
	}

	
	@Override
	public String createNewCommunityPolicy(Policy policy) {
		
		String newPolicyNumber = generateNewPolicyNumber(true);
		
		jdbcTemplate.update(
				"INSERT INTO COMMUNITY_POLICIES "
				+ "(NUMBER,SHARING_CAUSE,FROM_COUNTRY,TO_COUNTRY,"
				+ "INCEPTION_DATE,EXPIRE_DATE,PREMIUM,SHARING_STATUS)"
				+ "VALUES (?, ?, ?, ? , ?, ?, ?, ?)",
						newPolicyNumber, 
						policy.getSharingClause(),
						policy.getFromCountry(), 
						policy.getToCountry(), 
						policy.getInceptionDate(),
						policy.getExpireDate(),
						policy.getPremium(), 
						policy.getSharingStatus());
		
		jdbcTemplate.update("INSERT INTO SHARING_ENDORSEMENT_INFO "
				+ "(POLICY_NUMBER,ENDR_NUMBER,ENDR_DATE,ENDR_TYPE,"
				+ "FROM_DATE,TO_DATE,SHARED_BY,PREMIUM)"
				+ "VALUES (?, ?, ?, ? , ?, ?, ?, ?)",
				newPolicyNumber , 
				generateNewEndorosmentNumber(newPolicyNumber),
                new Date(),   
                "NoShare", 
                policy.getInceptionDate(), 
                policy.getExpireDate(), 
                null, 
                null);
		
		return newPolicyNumber;
	}
	
	private String generateNewPolicyNumber (boolean isCommunity) {

		Collection<Policy> all = isCommunity ? this.getAllCommunityPolicies() : 
			                                   this.getAllAllowedPolicies();
		String prefix = isCommunity ? "CMT-TR" : "LMG-TR";
		
		int prevNumber = 0;

		for (Policy buff: all) {
				String endNumber = buff.getPolicyNumber().substring(buff.getPolicyNumber().length() - 3);
				Integer number = Integer.valueOf(endNumber);
				if (prevNumber < number.intValue()) {
					prevNumber = number;
				}
		}
		

		return prefix + "-" + String.format("%03d", prevNumber + 1);
	}

	@Override
	public String createNewLibertyPolicy(Policy policy) {
		
    String newPolicyNumber = generateNewPolicyNumber(false);
		
		jdbcTemplate.update(
				"INSERT INTO ALLOWED_POLICIES  "
				+ "(NUMBER,SHARING_CAUSE,FROM_COUNTRY,TO_COUNTRY,"
				+ "INCEPTION_DATE,EXPIRE_DATE,PREMIUM,SHARING_STATUS)"
				+ "VALUES (?, ?, ?, ? , ?, ?, ?, ?)",
						newPolicyNumber, 
						policy.getSharingClause(),
						policy.getFromCountry(), 
						policy.getToCountry(), 
						policy.getInceptionDate(),
						policy.getExpireDate(),
						policy.getPremium(), 
						policy.getSharingStatus());
		
		jdbcTemplate.update("INSERT INTO SHARING_ENDORSEMENT_INFO "
				+ "(POLICY_NUMBER,ENDR_NUMBER,ENDR_DATE,ENDR_TYPE,"
				+ "FROM_DATE,TO_DATE,SHARED_BY,PREMIUM)"
				+ "VALUES (?, ?, ?, ? , ?, ?, ?, ?)",
				newPolicyNumber , 
				generateNewEndorosmentNumber(newPolicyNumber),
                new Date(),   
                "NoShare", 
                policy.getInceptionDate(), 
                policy.getExpireDate(), 
                null, 
                null);

		
		return newPolicyNumber;
	}

	@Override
	public void saveIndorosment(Endorsement info) {
		
		jdbcTemplate.update("INSERT INTO SHARING_ENDORSEMENT_INFO "
				+ "(POLICY_NUMBER,ENDR_NUMBER,ENDR_DATE,ENDR_TYPE,"
				+ "FROM_DATE,TO_DATE,SHARED_BY,PREMIUM)"
				+ "VALUES (?, ?, ?, ? , ?, ?, ?, ?)",
				info.getPolicyNumber() , 
				generateNewEndorosmentNumber(info.getPolicyNumber()),
				info.getEndorsendDate(),   
                info.getEndorsendType(), 
                info.getFromDate(), 
                info.getToDate(), 
                info.getSharedBy(), 
                info.getPremium());
		
	}
	

}
