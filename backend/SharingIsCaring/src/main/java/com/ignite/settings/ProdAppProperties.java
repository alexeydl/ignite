package com.ignite.settings;

import javax.annotation.PostConstruct;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

/**
 * Settings for PROD env 
 * @author alex
 *
 */
@Component
@Profile("PROD")
public class ProdAppProperties extends AppProperties {

	private final static Logger log = LoggerFactory.getLogger(DevAppProperties.class);

	@PostConstruct
	public void init() {

		try {
			props.load(ProdAppProperties.class.getResourceAsStream("/prod.properties"));

		} catch (Exception e) {
			log.error("Unable to load properties:");
			e.printStackTrace();
		}

	}
}