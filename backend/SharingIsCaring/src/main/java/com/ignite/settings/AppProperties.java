package com.ignite.settings;

import java.util.Properties;

/**
 * App settings
 * @author alex
 *
 */
public abstract class AppProperties {

	protected Properties props = new Properties();

	/**
	 * 
	 * @param name
	 * @return
	 */
	public String getProperty(String name) {
		return props.getProperty(name); 
	}

	/**
	 * 
	 * @param name
	 * @return
	 */
	public Long getPropertyLong(String name) {
		try {
			return props.getProperty(name) != null ? Long.valueOf(props.getProperty(name)) : null;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	/**
	 * 
	 * @param name
	 * @return
	 */
	public Integer getPropertyInteger(String name) {
		try {
			return props.getProperty(name) != null ? Integer.valueOf(props.getProperty(name)) : null;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

}