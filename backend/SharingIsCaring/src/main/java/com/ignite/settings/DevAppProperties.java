package com.ignite.settings;

import javax.annotation.PostConstruct;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;


/**
 * Settings for DEV env
 * @author alex
 *
 */
@Component
@Profile("DEV")
public class DevAppProperties extends AppProperties {
	
	private final static Logger log = LoggerFactory.getLogger(DevAppProperties.class);
	
	@PostConstruct
	public void init() {

		try {
			props.load(DevAppProperties.class.getResourceAsStream("/dev.properties"));
			
		} catch (Exception e) {
			log.error("Unable to load properties:");
			e.printStackTrace();
		}

	}
}