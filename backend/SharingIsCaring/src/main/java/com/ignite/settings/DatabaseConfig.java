package com.ignite.settings;

import javax.sql.DataSource;

import org.apache.commons.dbcp2.BasicDataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Database config 
 * @author alex
 *
 */
@Configuration
public class DatabaseConfig {

	@Autowired
	private AppProperties appConfig;

	@Bean
	public DataSource dataSource() {

		BasicDataSource result = new BasicDataSource();
		
		result.setDriverClassName("com.mysql.jdbc.Driver");
		String dbUrl = appConfig.getProperty("DB_URL");
		result.setUrl(dbUrl);
		result.setUsername(appConfig.getProperty("DB_USER"));
		result.setPassword(appConfig.getProperty("DB_PASSWORD"));

		return result;
	}

	public AppProperties getAppConfig() {
		return appConfig;
	}

	public void setAppConfig(AppProperties appConfig) {
		this.appConfig = appConfig;
	}
}