package com.ignite.controller;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.ignite.entity.SharePolicyRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ignite.entity.CalcPremiumRequest;
import com.ignite.entity.CalculationResponse;
import com.ignite.entity.Country;
import com.ignite.entity.Endorsement;
import com.ignite.entity.InsurerResponse;
import com.ignite.entity.LineOfBusinessResponse;
import com.ignite.entity.Policy;
import com.ignite.entity.PolicyCreateRequest;
import com.ignite.entity.PolicyCreateResponse;
import com.ignite.entity.PolicyFullInfo;
import com.ignite.entity.StatusResponse;
import com.ignite.repository.MysqlRepository;
import com.ignite.security.TokenBasedSecurityContext;
import com.ignite.service.CommonDataService;
import com.ignite.settings.AppProperties;

/**
 * Main REST controller
 * @author alex
 *
 */
@RestController
public class MainController {
	
	@Autowired
	private AppProperties props;
	@Autowired
	private MysqlRepository mysqlRepository;
	@Autowired
	private TokenBasedSecurityContext securityContext;
	@Autowired
	private CommonDataService commonDataService;
	
	private static final Logger logger = LoggerFactory.getLogger(MainController.class);

	/**
	 * 
	 * @return
	 */
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String test() {
		return "Sharing Is Caring. REST API v.0.1";
	}
	
	/**
	 * 
	 * @return
	 */
	@CrossOrigin
	@RequestMapping(value = "/insurers_for_sharing", method = RequestMethod.GET)
	public List<InsurerResponse> getInsurersForSharing(@RequestParam String session, 
			HttpServletRequest request, HttpServletResponse response) {
		
		logger.info("insurers_for_sharing invoked");
		
		try {
			
			if (securityContext.getSession(session) == null) {
				logger.info("Auth failed.");
				response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
				return null;
			}
			
			List<InsurerResponse> result = commonDataService.getInsurersForSharing();
			logger.info("insurers_for_sharing completed");
			return result;
			
		} catch (Exception e){
			e.printStackTrace();
			response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
			return null;
		}
	}
	
	@CrossOrigin
	@RequestMapping(value = "/lobs_by_insurer", method = RequestMethod.GET)
	public List<LineOfBusinessResponse> getLobsByInsurer(@RequestParam String insurer_code,
			@RequestParam String session, HttpServletRequest request, HttpServletResponse response) {

		logger.info("lobs_by_insurer invoked");

		try {

			if (securityContext.getSession(session) == null) {
				logger.info("Auth failed.");
				response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
				return null;
			}

			List<LineOfBusinessResponse> result = commonDataService.getLobsByInsurer(insurer_code);

			logger.info("lobs_by_insurer completed");
			return result;

		} catch (Exception e) {
			e.printStackTrace();
			response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
			return null;
		}
	}
	
	@CrossOrigin
	@RequestMapping(value = "/policy_allowed_status", method = RequestMethod.GET)
	public StatusResponse getPolicyAllowedStatus(@RequestParam String policy_number,
			@RequestParam String session,HttpServletRequest request, HttpServletResponse response) {
		
		logger.info("policy_allowed_status invoked");
		
		try {
			
			if (securityContext.getSession(session) == null) {
				logger.info("Auth failed.");
				response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
				return null;
			}
			
			StatusResponse result = 
					commonDataService.getPolicyAllowedStatus(policy_number, 
							securityContext.getSession(session).getLogin());
			
			logger.info("policy_allowed_status completed");
			return result;
			
		} catch (Exception e){
			e.printStackTrace();
			response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
			return null;
		}
	}
	
	/**
	 *
	 * @param rq
	 */
	@CrossOrigin
	@RequestMapping(value = "/policy_sharing", method = RequestMethod.POST)
	public void getPolicyAllowedStatus(@RequestBody SharePolicyRequest rq, 
			HttpServletRequest request, HttpServletResponse response) {

		logger.info("policy_sharing invoked");

		try {

			if (securityContext.getSession(rq.getSession()) == null) {
				logger.info("Auth failed.");
				response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
				return;
			}

			commonDataService.updatePolicySharing(rq, securityContext.getSession(rq.getSession()));

			logger.info("policy_sharing completed");

		} catch (Exception e){
			response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
			e.printStackTrace();
		}
	}
	
	
	@CrossOrigin
	@RequestMapping(value = "/from_countries", method = RequestMethod.GET)
	public List<Country> getFromCountries(String session, HttpServletRequest request, HttpServletResponse response) {
		
		logger.info("from_countries invoked");
		
		List<Country> result = new ArrayList<Country>();
		
		try {
			
			if (securityContext.getSession(session) == null) {
				logger.info("Auth failed.");
				response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
				return null;
			}
			
			for (Policy data: commonDataService.getAllPolicies()) {
				
				Country temp = new Country();
				
				temp.setName(data.getFromCountry());
				
				if (!result.contains(temp)) {
					result.add(temp);
				}
			}
			
			logger.info("from_countries completed");
			return result;
			
		} catch (Exception e){
			e.printStackTrace();
			response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
			return null;
		}
	}
	
	
	@CrossOrigin
	@RequestMapping(value = "/to_countries", method = RequestMethod.GET)
	public List<Country> getToCountries(String session, HttpServletRequest request, HttpServletResponse response) {
		
		logger.info("to_countries invoked");
		
		List<Country> result = new ArrayList<Country>();
		
		try {
			
			if (securityContext.getSession(session) == null) {
				logger.info("Auth failed.");
				response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
				return null;
			}
			
			for (Policy data: commonDataService.getAllPolicies()) {
				
				Country temp = new Country();
				
				temp.setName(data.getToCountry());
				
				if (!result.contains(temp)) {
					result.add(temp);
				}
			}
			
			logger.info("to_countries completed");
			return result;
			
		} catch (Exception e){
			e.printStackTrace();
			response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
			return null;
		}
	}
	
	@CrossOrigin
	@RequestMapping(value = "/community_calculation", method = RequestMethod.POST)
	public CalculationResponse calculate(@RequestBody CalcPremiumRequest rq, 
			HttpServletRequest request, HttpServletResponse response) {

		logger.info("community_calculation invoked");

		try {

			if (securityContext.getSession(rq.getSession()) == null) {
				logger.info("Auth failed.");
				response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
				return null;
			}
		
			List<String> selectedPolicies = new ArrayList<String> ();
			
			for(Endorsement buff: commonDataService.getAllSharingHistory()){
				if (buff.getEndorsendType().trim().equalsIgnoreCase("Share")) {
					if (!buff.getFromDate().after(rq.getDateStart())) 
					if (!buff.getToDate().before(rq.getDateEnd())){
						selectedPolicies.add(buff.getPolicyNumber().trim().toUpperCase());
					}
				}
			}
			
			Double prem = null;
			String basedPolicy = null;
			
			for (Policy buff: commonDataService.getAllPolicies()) {
		        if (selectedPolicies.contains(buff.getPolicyNumber().trim().toUpperCase())){
		        	
		        	Policy selectedPoicy = commonDataService.getPolicyByNumber(buff.getPolicyNumber());
		        	
		        	if (selectedPoicy.getFromCountry().trim().equalsIgnoreCase(rq.getCountryFrom().trim()))
		        	if (selectedPoicy.getToCountry().trim().equalsIgnoreCase(rq.getCountryTo().trim()))
		            if (prem == null || prem.compareTo(buff.getPremium().doubleValue()) == 1) {
		            	prem = buff.getPremium().doubleValue();
		            	basedPolicy = buff.getPolicyNumber();
		            }
		        }
			}
			
			if (prem == null)
				return null;
			
			//prem 
			long daysCount  = getDifferenceDays(rq.getDateStart(), rq.getDateEnd());
			prem = prem / 365.0 * daysCount;
		
			CalculationResponse result = new CalculationResponse();
			result.setCommuntiyPrem(BigDecimal.valueOf(prem).setScale(2, BigDecimal.ROUND_DOWN));
			result.setBasedNumber(basedPolicy);
			
			logger.info("community_calculation completed");
			
			return result;

		} catch (Exception e){
			response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
			e.printStackTrace();
		}
		
		return null;
	}
	
	@CrossOrigin
	@RequestMapping(value = "/compare_to_others", method = RequestMethod.POST)
	public CalculationResponse compareToOthers(@RequestBody CalcPremiumRequest rq, 
			HttpServletRequest request, HttpServletResponse response) {

		logger.info("compare_to_others invoked");

		try {

			if (securityContext.getSession(rq.getSession()) == null) {
				logger.info("Auth failed.");
				response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
				return null;
			}
		
			
			CalculationResponse result = null;
					
		    try {
		    	result = this.calculate(rq, request, response);
		    } catch (Exception e){
		    	e.printStackTrace();
		    }
			
		    if (result != null && result.getCommuntiyPrem() != null) {
		    	
		    	double communityPrem = result.getCommuntiyPrem().doubleValue();
		    	double liberty = communityPrem * 1.10;
		    	double aig = communityPrem * 1.15;
		    	double geico = communityPrem * 1.20;
		    	
		    	result.setAigPrem(BigDecimal.valueOf(aig).setScale(2, BigDecimal.ROUND_DOWN));
		    	result.setLibertyPrem(BigDecimal.valueOf(liberty).setScale(2, BigDecimal.ROUND_DOWN));
		    	result.setGeicoPrem(BigDecimal.valueOf(geico).setScale(2, BigDecimal.ROUND_DOWN));
		    } else {
		    	
		    	result = new CalculationResponse();
		    	Policy randomPolicy = commonDataService.getAllPolicies().iterator().next();
		    	long daysCount  = getDifferenceDays(rq.getDateStart(), rq.getDateEnd());
		    	
				double communityPrem = randomPolicy.getPremium().doubleValue() / 365.0 * daysCount;
		    	double liberty = communityPrem * 1.10;
		    	double aig = communityPrem * 1.15;
		    	double geico = communityPrem * 1.20;
		    	
		    	result.setAigPrem(BigDecimal.valueOf(aig).setScale(2, BigDecimal.ROUND_DOWN));
		    	result.setLibertyPrem(BigDecimal.valueOf(liberty).setScale(2, BigDecimal.ROUND_DOWN));
		    	result.setGeicoPrem(BigDecimal.valueOf(geico).setScale(2, BigDecimal.ROUND_DOWN));
		    }
			
			logger.info("compare_to_others completed");
			
			return result;

		} catch (Exception e){
			response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
			e.printStackTrace();
		}
		
		return null;
	}
	
	@CrossOrigin
	@RequestMapping(value = "/new_policy", method = RequestMethod.POST)
	public PolicyCreateResponse createNewPolicy(@RequestBody PolicyCreateRequest rq,	
			HttpServletRequest request, HttpServletResponse response) {

		   logger.info("new_policy invoked");

		try {

			if (securityContext.getSession(rq.getSession()) == null) {
				logger.info("Auth failed.");
				response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
				return null;
			}
		
			PolicyCreateResponse result = new PolicyCreateResponse();
			
			Policy policy = new Policy();
			
			policy.setFromCountry(rq.getFromCountry().trim().toUpperCase());
			policy.setToCountry(rq.getToCountry().trim().toUpperCase());
			policy.setPolicyHolder(securityContext.getSession(rq.getSession()).getLogin());
			policy.setExpireDate(rq.getExpireDate());
			policy.setInceptionDate(rq.getInceptionDate());
			
			if (rq.getPolicyType() != null && rq.getPolicyType().equalsIgnoreCase("community")) {
				
				policy.setSharingClause("N");
				policy.setSharingStatus("N");
				policy.setPremium(rq.getPremium());
				policy.setPolicyNumber(commonDataService.createNewCommunityPolicy(policy));
				
				Endorsement info = new Endorsement();
				info.setEndorsementNumber(commonDataService.generateNewEndorosmentNumber(rq.getBasedPolicy()));
				info.setPolicyNumber(rq.getBasedPolicy());
				info.setPremium(rq.getPremium());
				info.setEndorsendType("Used");
				info.setSharedBy(policy.getPolicyNumber());
				info.setFromDate(rq.getInceptionDate());
				info.setToDate(rq.getExpireDate());
				info.setEndorsendDate(new Date());
				
				//2 endorosments for communtiy policy
			    commonDataService.saveIndorosment(info);
			    
			    info = new Endorsement();
				info.setEndorsementNumber(commonDataService.generateNewEndorosmentNumber(rq.getBasedPolicy()));
				info.setPolicyNumber(rq.getBasedPolicy());
				info.setPremium(null);
				info.setEndorsendType("NoShare");
				info.setSharedBy(null);
				info.setFromDate(rq.getInceptionDate());
				info.setToDate(rq.getExpireDate());
				info.setEndorsendDate(new Date());
				
			    commonDataService.saveIndorosment(info);
			    
				
			} else {
				policy.setSharingClause("Y");
				policy.setSharingStatus("N");
				double yearPrem = rq.getPremium().doubleValue() * 365.0;
				policy.setPremium(BigDecimal.valueOf(yearPrem).setScale(2, BigDecimal.ROUND_DOWN));
				
				policy.setPolicyNumber(commonDataService.createNewLibertyPolicy(policy));
			}
			
			
			result.setPolicyNumber(policy.getPolicyNumber());
			
			logger.info("new_policy completed");
			
			
			return result;

		} catch (Exception e){
			response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
			e.printStackTrace();
		}
		
		return null;
	}
	
	@CrossOrigin
	@RequestMapping(value = "/shared_policies", method = RequestMethod.GET)
	public List<PolicyFullInfo> getPolicies(@RequestParam Boolean sharing, @RequestParam String session,
			HttpServletRequest request, HttpServletResponse response) {
		
		logger.info("shared_policies invoked");
		
		try {
			
			if (securityContext.getSession(session) == null) {
				logger.info("Auth failed.");
				response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
				return null;
			}
			
			List<Policy> all = commonDataService.getAllPolicies();
			List<PolicyFullInfo> result = new ArrayList<PolicyFullInfo>();
			
			for (Policy buff: all){
				
				if (sharing) {
					if (true) {
						
						PolicyFullInfo full = new PolicyFullInfo();
						
						full.setExpireDate(buff.getExpireDate());
						full.setFromCountry(buff.getFromCountry());
						full.setInceptionDate(buff.getInceptionDate());
						full.setPolicyHolder(buff.getPolicyHolder());
						full.setPolicyNumber(buff.getPolicyNumber());
						full.setPremium(buff.getPremium());
						full.setSharingClause(buff.getSharingClause());
						full.setSharingStatus(buff.getSharingStatus());
						full.setToCountry(buff.getToCountry());
						
						try {
							for (Endorsement end: commonDataService.getAllSharingHistory()){
								
								 if (!end.getEndorsendType().trim().equals("Used"))  
									 break;
								
								 if (end.getPolicyNumber() != null && 
										 end.getPolicyNumber().trim().equalsIgnoreCase(buff.getPolicyNumber().trim())) {
									 full.setCount(full.getCount()+1);
									 
									 if (full.getSavings() == null)
										 full.setSavings(BigDecimal.valueOf(0.00));
									 
									 if (buff.getPremium() != null)
									 full.setSavings(buff.getPremium().add(full.getSavings()));
									 
								 }
							}
						} catch (Exception e){
							e.printStackTrace();
						}
						
						result.add(full);
					}	
				} 
			}
			
			logger.info("shared_policies completed");
			return result;
			
		} catch (Exception e){
			e.printStackTrace();
			response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
			return null;
		}
	}

	
	public AppProperties getProps() {
		return props;
	}
	public void setProps(AppProperties props) {
		this.props = props;
	}

	public MysqlRepository getMysqlRepository() {
		return mysqlRepository;
	}

	public void setMysqlRepository(MysqlRepository mysqlRepository) {
		this.mysqlRepository = mysqlRepository;
	}

	/**
	 * @return the securityContext
	 */
	public TokenBasedSecurityContext getSecurityContext() {
		return securityContext;
	}

	/**
	 * @param securityContext the securityContext to set
	 */
	public void setSecurityContext(TokenBasedSecurityContext securityContext) {
		this.securityContext = securityContext;
	}

	/**
	 * @return the commonDataService
	 */
	public CommonDataService getCommonDataService() {
		return commonDataService;
	}

	/**
	 * @param commonDataService the commonDataService to set
	 */
	public void setCommonDataService(CommonDataService commonDataService) {
		this.commonDataService = commonDataService;
	}	
	
	private long getDifferenceDays(Date d1, Date d2) {
	    long diff = d2.getTime() - d1.getTime();
	    return TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
	}
}
