function loadFrom() {

 $.getJSON('http://ignite-libertyrus.rhcloud.com/from_countries?session=3454567543', function (data) {

    console.log(data);
    $('#from-country-select').find('option').remove();
    $('#from-country-select').prop("selectedIndex", -1);

    $.each(data, function(i, value) {
    	if (i == 0)
        $('#from-country-select').append($('<option selected>').text(value.name).attr('value', value.name));
        else
        $('#from-country-select').append($('<option>').text(value.name).attr('value', value.name));
    });

    $('#from-country-select').selectmenu('refresh');
    console.log('from data filled');

 });

}

function loadTo() {

 $.getJSON('http://ignite-libertyrus.rhcloud.com/to_countries?session=3454567543', function (data) {

    console.log(data);
    $('#to-country-select').find('option').remove();
    $('#to-country-select').prop("selectedIndex", -1);

    $.each(data, function(i, value) {
    	if (i == 0)
        $('#to-country-select').append($('<option selected>').text(value.name).attr('value', value.name));
        else
        $('#to-country-select').append($('<option>').text(value.name).attr('value', value.name));
    });

    $('#to-country-select').selectmenu('refresh');
    console.log('to data filled');

 });
}
function setToDate() {
    var endDate = $('#tomorrowDate').attr('value');
    var startDate = $('#theDate').attr('value');
	var myDate = new Date(startDate);
    myDate.setDate(myDate.getDate() + 1);
   $('#tomorrowDate').attr('value', myDate.toISOString().slice(0, 10));
}
function setFromDate() {
   //TODO:
}

function changeDates() {

    var today = new Date();
    var tomorrow = new Date(new Date().getTime() + 24 * 60 * 60 * 1000);
    var dd = today.getDate();
    var mm = today.getMonth()+1; //January is 0!
    var yyyy = today.getFullYear();
    var tomday = tomorrow.getDate();
    var tommonth = tomorrow.getMonth() + 1;
    var tomyear = tomorrow.getFullYear();

    if(dd<10){dd='0'+dd} if(mm<10){mm='0'+mm} today = yyyy+'-'+mm+'-'+dd;
    if(tomday<10){tomday='0'+tomday} if(tommonth<10){tommonth='0'+tommonth} tomorrow = tomyear+'-'+tommonth+'-'+tomday;
    $('#theDate').attr('value', today);
    $('#tomorrowDate').attr('value', tomorrow);	

}
function generateDates() {

    var flexDays = new Number($('#flex-days').attr('value'));
	var startDate = $('#theDate').attr('value');
	var endDate = $('#tomorrowDate').attr('value');
    
	var myDate = new Date(startDate);
    myDate.setDate(myDate.getDate());
    var formattedDate = myDate.toISOString().slice(0, 10);
    //from date
    $('#date-from-2').text(formattedDate);	


    myDate = new Date(startDate);
    myDate.setDate(myDate.getDate() - flexDays);
    formattedDate = myDate.toISOString().slice(0, 10);
    //from date - offset
    $('#date-from-1').text(formattedDate);


    myDate = new Date(startDate);
    myDate.setDate(myDate.getDate() + flexDays);
    formattedDate = myDate.toISOString().slice(0, 10);
    //from date + offset
    $('#date-from-3').text(formattedDate);


    myDate = new Date(endDate)
    myDate.setDate(myDate.getDate());
    var formattedDate = myDate.toISOString().slice(0, 10);
    //to date
    $('#date-to-2').text(formattedDate);


    myDate = new Date(endDate)
    myDate.setDate(myDate.getDate() - flexDays );
    var formattedDate = myDate.toISOString().slice(0, 10);
    //to date - offset
    $('#date-to-1').text(formattedDate);


    myDate = new Date(endDate)
    myDate.setDate(myDate.getDate() + flexDays );
    var formattedDate = myDate.toISOString().slice(0, 10);
    //to date - offset
    $('#date-to-3').text(formattedDate);	


}

function compareToOthersByPrice(price) {
	    $("#liberty-prem").text(Math.round(new Number(price*1.1*100)) / 100);
    	$("#aig-prem").text(Math.round(new Number(price*1.15*100)) / 100);
    	$("#geico-prem").text(Math.round(new Number(price*1.20*100)) / 100);
}

function compareToOthers(dateStart, dateEnd) {

	var countryFrom = $('#from-country-select option:selected').text();
	var countryTo = $('#to-country-select option:selected').text();


	var dataToSend = new Object();

	dataToSend.dateStart = dateStart;
	dataToSend.dateEnd = dateEnd;
	dataToSend.session = '3454567543';
	dataToSend.countryTo = countryTo;
	dataToSend.countryFrom = countryFrom;
	dataToSend.basedPolicy = null;

	$.ajax({

    type: 'POST',
    url: 'http://ignite-libertyrus.rhcloud.com/compare_to_others ',
    crossDomain: true,
    data: JSON.stringify(dataToSend),
    dataType: 'json',
    contentType: "application/json",
    success: function(responseData) {

    	$("#liberty-prem").text(responseData.libertyPrem);
    	$("#aig-prem").text(responseData.aigPrem);
    	$("#geico-prem").text(responseData.geicoPrem);

    },
    error: function (responseData) {
    	this.result = null;
        console.log(responseData);
    }
    });
}

function calculatePolicy(dateStart, dateEnd, objectToSetText) {

	var countryFrom = $('#from-country-select option:selected').text();
	var countryTo = $('#to-country-select option:selected').text();


	var dataToSend = new Object();

	dataToSend.dateStart = dateStart;
	dataToSend.dateEnd = dateEnd;
	dataToSend.session = '3454567543';
	dataToSend.countryTo = countryTo;
	dataToSend.countryFrom = countryFrom;

	$.ajax({

    type: 'POST',
    url: 'http://ignite-libertyrus.rhcloud.com/community_calculation',
    crossDomain: true,
    data: JSON.stringify(dataToSend),
    dataType: 'json',
    contentType: "application/json",
    success: function(responseData) {

         console.log(responseData);
    	 
    	 if (responseData == null || responseData.communtiyPrem === undefined 
    	 	 || responseData.communtiyPrem === null || responseData.communtiyPrem <= 0) {
    	 	objectToSetText.text('-');
    	 } else {
    	 	objectToSetText.text(responseData.communtiyPrem);
    	 	objectToSetText.addClass(responseData.basedNumber);
    	 }

    	 $('.prem-cell').removeClass('selected-cell');
    	 $('#liberty-prem').removeClass('selected-cell');

         if (!($('#prem-2-2').text().toString() == '-'.toString())) {
     	    $('#prem-2-2').addClass('selected-cell');
     	    compareToOthers($('#date-from-2').text(), $('#date-to-2').text());
         } else {
         	$('#liberty-prem').addClass('selected-cell');
         }
    	 
    },
    error: function (responseData) {
    	this.result = null;
        console.log(responseData);
    }
    });
}

function calculateAll() {

    calculatePolicy($('#date-from-1').text(), $('#date-to-1').text(), $('#prem-1-1'));
    calculatePolicy($('#date-from-2').text(), $('#date-to-1').text(), $('#prem-1-2'));
    calculatePolicy($('#date-from-3').text(), $('#date-to-1').text(), $('#prem-1-3'));

    calculatePolicy($('#date-from-1').text(), $('#date-to-2').text(), $('#prem-2-1'));
    calculatePolicy($('#date-from-2').text(), $('#date-to-2').text(), $('#prem-2-2'));
    calculatePolicy($('#date-from-3').text(), $('#date-to-2').text(), $('#prem-2-3'));

    calculatePolicy($('#date-from-1').text(), $('#date-to-3').text(), $('#prem-3-1'));
    calculatePolicy($('#date-from-2').text(), $('#date-to-3').text(), $('#prem-3-2'));
    calculatePolicy($('#date-from-3').text(), $('#date-to-3').text(), $('#prem-3-3'));
}

function checkCommunityOffer() {
	$("#calc-table").removeClass('hide');
	$("#compare").removeClass('hide');
	checkClicked = true;
	generateDates();
	calculateAll();		
}

function recalc() {
	if (checkClicked) {
		checkCommunityOffer();
	}
}

function selectItemTd(td) {

	if (!($(td).text().toString() == '-'.toString())) {
        $('.prem-cell').removeClass('selected-cell');
	    $(td).addClass('selected-cell');
	    compareToOthersByPrice($(td).text());
	    $('#liberty-prem').removeClass('selected-cell');
    }

}


function createNewPolicy(dateStart, dateEnd, policyType, sharingClause, premium, based) {

	var countryFrom = $('#from-country-select option:selected').text();
	var countryTo = $('#to-country-select option:selected').text();


	var dataToSend = new Object();

	dataToSend.inceptionDate = dateStart;
	dataToSend.expireDate = dateEnd;
	dataToSend.session = '3454567543';
	dataToSend.toCountry = countryTo;
	dataToSend.fromCountry = countryFrom;
	dataToSend.policyType = policyType;
	dataToSend.sharingClause = sharingClause;
	dataToSend.sharingStatus = "N";
	dataToSend.premium = premium;
	dataToSend.basedPolicy = based;

	console.log(dataToSend);


	$.ajax({

    type: 'POST',
    url: 'http://ignite-libertyrus.rhcloud.com/new_policy',
    crossDomain: true,
    data: JSON.stringify(dataToSend),
    dataType: 'json',
    contentType: "application/json",
    success: function(responseData) {
    	alert("Your policy with number " + responseData.policyNumber + " has been created!");
    },
    error: function (responseData) {
    	this.result = null;
        console.log(responseData);
    }
    });
}

function takeIt() {
    
    var policyType = $('#liberty-prem').hasClass('selected-cell') ? "liberty" : "community";
    var sharingClause = $('#liberty-prem').hasClass('selected-cell') ? "Y" : "N";
    var dateEnd = $('#tomorrowDate').attr('value');
    var dateStart = $('#theDate').attr('value');
    var premium = new Number($(".selected-cell").text());
    var based = $(".selected-cell").attr('class');
    based = based.match(/(\bL\S+\b)/ig);
    createNewPolicy(dateStart, dateEnd, policyType, sharingClause, premium, based != null ? based[0] : null);
    
}










