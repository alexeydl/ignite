/**
 * 
 */
'use strict';
var app= angular.module('validate',['ui.router']);

app.config(function($stateProvider,$urlRouterProvider){
	$stateProvider
	.state('Validate',{
		 url:'/Validate',
		 TemplateUrl:'Validate.html',
		 controller: 'ValidateController',
		 controlleras: 'Validate'
	});
	
	urlRouterProvider.otherwise('/Validate');
});